#!/usr/bin/php -q
<?php
	$base = "/www/domainomatics.com/cron";
	include($base."/tldextract.php");
	include($base."/../www/config/config.php");

	$db = new PDO('mysql:host='.databaseServer.';dbname='.database,databaseUser,databasePass);

	$config = array(
		'allow-hyphens'=>FALSE,
		'allow-numbers'=>FALSE,
		'tlds'=>array(
			'com',
			'net',
			'org'
		),
		'age'=>86400 //Will expire in under 24 hours.
	);

	$source_url = "http://www.pool.com/Downloads/PoolDeletingDomainsList.zip";
	$cont = file_get_contents($source_url);
	$fp = fopen($base."/work/work.zip","wb");
	fputs($fp,$cont);
	fclose($fp);
	$zip = new ZipArchive;
	$res = $zip->open($base.'/work/work.zip');
	$targets = array();
	if ($res === TRUE) {
		$list = explode("\n",$zip->getFromName('PoolDeletingDomainsList.txt'));
		foreach($list as $line) {
			$ver = expiring_verify($line);
			if ($ver === FALSE) {
			} else {
				$handle = $db->prepare("INSERT into que (domain,expires,processed) VALUES (:target,:expires,0)");
				$handle->execute(array(
					':target'=>expiring_extract($line),
					':expires'=>$ver
				));
			}
		}
	} else {
		die('Unable to read file!');
	}

	function expiring_verify($line) {
		global $config;
		$parts = str_getcsv($line,",");
		$domain = $parts[0];
		$epoch = strtotime($parts[1]);

		$diff = $epoch - time();
		$pass = true;


		$comp = tldextract($domain);
		$tld = $comp['tld'];

		if ($diff >= $config['age']) $pass = false;
		if ((!$config['allow-hyphens']) and (strstr($domain,"-"))) $pass = false;
		if ((!$config['allow-numbers']) and (preg_match('#[0-9]#',$domain))) $pass = false;
		if (!in_array(strtolower($tld),$config['tlds'])) $pass = false;
		if ($pass === false) return false;
		return $epoch;
	}

	function expiring_extract($line) {
		$parts = str_getcsv($line,",");
		return $parts[0];
	}

?>
