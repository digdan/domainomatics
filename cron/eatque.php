#!/usr/bin/php -q
<?php
	set_time_limit(0);
	const LIMIT=50;
	chdir("/www/domainomatics.com/cron");

	include("../www/config/config.php");
	include "../www/includes/functions.php";
	include "../www/includes/NEXstats.php";
	$myPID = getmypid();

	$db = new PDO('mysql:host='.databaseServer.';dbname='.database,databaseUser,databasePass);

	$query = "SELECT id FROM que WHERE processed = 0 ORDER by expires DESC LIMIT ".LIMIT;
	$res = $db->query($query);
	foreach($res as $item) {
		$squery = "UPDATE que SET flags = 3, pid = ".$myPID." WHERE id = ".$item['id'];
		$db->query($squery);
	}

	$query = "SELECT * from que WHERE flags = 3 and pid = ".$myPID;
	$res = $db->query($query);
	foreach($res as $item) {
		$url = $item['domain'];
		$url       = rtrim(trim($item['domain']), ".");
		$url       = getdomain($url);
		$flags = 0;
		$pass = true;

		if (urlExists($url)) {
			continue;
		} else if (validURL($url)) {
			$pass = true;
		} else if (!validURL($url)) {
			$flags = 2;
			$pass = false;
		}

		if ($pass) {
			echo "Targeting {$url}.\n";
			echo "\tCreating records.\n";
			saveURL($url);
			setDropDate($url,$item['expires']);

			echo "\tTraffic Stats.\n";
			calculateTrafficStats($url);

			echo "\tSERPS Stats.\n";
			searchEnginesIndexStats($url);

			echo "\tRanking Stats.\n";
			rankingStats($url);

			echo "\tSaftey Stats.\n";
			websiteSafetyStats($url);

			echo "\tSEO Stats.\n";
			seoStats($url);

			echo "\tSocial Stats.\n";
			socialMediaStats($url);

			echo "\tDomain Stats.\n";
			domainInformation($url);

			echo "\tServer Stats.\n";
			webServerInformation($url);

			echo "\tWorth.\n";
			calculateWorth($url, $language);
			countSearches();
		}

		$handle = $db->prepare("UPDATE que set processed = :now,flags=:flags WHERE id = :id");
		$handle->execute(array(
			':now'=>time(),
			':flags'=>$flags,
			':id'=>$item['id']
		));
	}
?>
