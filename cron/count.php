#!/usr/bin/php -q
<?php
set_time_limit(0);
/*
 * Average day requires 65k listings
 *
 *  If scripts runs every 15 minutes, thats 4 times per hour, or 96 times a day.
 *      60,000 / 96 = 625
 *
 *  Speeds currently allow 1 per 10 seconds,
 *  15 minutes * 60 seconds = 900 seconds / 10 seconds = 90 per limit
 *
 *      We would only be valuating 14% or listings. , need 7 times as many. Or 7 new workers.
 *
 */
const LIMIT=90;
chdir("/www/domainomatics.com/cron");

include("../www/config/config.php");
include "../www/includes/functions.php";
include "../www/includes/NEXstats.php";

$db = new PDO('mysql:host='.databaseServer.';dbname='.database,databaseUser,databasePass);
$query = "SELECT count(id) as cnt FROM que WHERE processed > 0";
$res = $db->query($query);
$ret = $res->fetch();
echo "Total records : ";
echo $ret['cnt'];
echo "\n";