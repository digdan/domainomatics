<?php
function isDomainAvailable($domain, $reverse = false) {
	global $error;
/*
	global $ext, 
		$error, 
		$allowedTLDs, 
		$language, 
		$buffer, 
		$enableGetHostByNameChecking, 
		$enableCheckDnsRRChecking, 
		$enableWhoisDomainChecking, 
		$enableAutoDomainDetection;
*/
	$ext = getServers();
	$allowedTLDs=NULL;
	$language = array(
		'tld_not_allowed'=>"TLD Not Allowed",
		'invalid_domain'=>"Invalid Domain",
		'unknown_tld'=>"Unknown TLD"
	);
	$buffer='';
	$enableGetHostByNameChecking = false;
	$enableCheckDnsRRChecking = false;
	$enableWhoisDomainChecking = true;
	$enableAutoDomainDetection = true;

	
	
	if ($enableWhoisDomainChecking == false) $enableAutoDomainDetection = false;
	
	$domain = trim($domain);
	if (preg_match('/^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)*[a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?$/i', $domain) != 1)	{
		$error = $language['invalid_domain'] . ' (' . $domain . ')';
		return false;
	}

	preg_match('@^(http://www\.|http://|www\.)?([^/]+)@i', $domain, $matches);
	$buffer = '';
	$domain = $matches[2];
	$domainParts = explode('.', $domain);
	$tld = '';
	if (count($domainParts) == 3)	{
		$tld = strtolower($domainParts[1] . '.' . $domainParts[2]);
	}	elseif (count($domainParts) == 2) {
		$tld = strtolower($domainParts[1]);
	} else {
		$error = $language['invalid_domain'];
		return false;
	}
	
	if ($allowedTLDs != null) {
		$found = false;
		foreach($allowedTLDs as $atld) {
			if ($atld == $tld) {
				$found = true;
				break;
			}
		}		
		if (!$found) {
			$error = $tld . $language['tld_not_allowed'];
			return false;
		}
	}

	$unknownTLD = false;
	if (!array_key_exists('.'.$tld,$ext)) {
		if ($enableAutoDomainDetection === false ) {
			$error = $language['unknown_tld'] . $tld;
			return false;
		}
		$unknownTLD = true;
	}

	if ($reverse === false) {
		if ($enableCheckDnsRRChecking) {
			if (function_exists('checkdnsrr')) {
				if (checkdnsrr($domain) !== false) return false;
				if (checkdnsrr($domain, 'A') !== false) return false;
			}
		}

		if ($enableGetHostByNameChecking) {
			$RE22CBD8984E1727D0A587413D72A88CF = gethostbyname($domain);
			if (($RE22CBD8984E1727D0A587413D72A88CF != $domain) && ($RE22CBD8984E1727D0A587413D72A88CF != '208.67.219.132')) {
				return false;
			}
		}
		
		if (!$enableWhoisDomainChecking) {
			return array(
				'true',
				$domainParts
			);
		}
	}

	$server = '';
	if (isset($_REQUEST['opendns'])) {
		echo '208.67.219.13';
		exit;
	}

	if ($unknownTLD) {
		$domainParts = explode('.', $tld);
		if (count($domainParts) > 1) $server = $domainParts[1] . '.whois-servers.net';
		else $server = $tld . '.whois-servers.net';
		$R7B8A9F2F48B874D40BD75BDD12F02557 = @gethostbyname($tld . '.whois-servers.net');
	} else {
		$server = $ext['.' . $tld][0];
		$R7B8A9F2F48B874D40BD75BDD12F02557 = @gethostbyname($server);
	}

	if ($tld == 'es') {
		$error = 'Error: ES not supported. They don\'t have a public whois server :(';
		return false;
	}

	if ($tld == 'au') {
		$server = $ext['.com.au'][0];
		$R7B8A9F2F48B874D40BD75BDD12F02557 = @gethostbyname($server);
	}

	if ($R7B8A9F2F48B874D40BD75BDD12F02557 == $server) {
		$error = 'Error: Invalid extension - ' . $tld . '. Or server has outgoing connections blocked to ' . $server . '.  Domain does not have DNS entry, so chances are high it is available.';
		return false;
	}

	$RAD10634E7F72CAA071320F21AEE5930D = @fsockopen($server, 43, $R32D00070D4FFBCCE2FC669BBA812D4C2, $RE5840D3E86DCF8489051E4F70C757552, 10);
	if ($R32D00070D4FFBCCE2FC669BBA812D4C2 == '10060') {
		$error = 'Error: Invalid extension - ' . $tld . ' (or whois server is down). Domain does not have DNS entry, so chances are high it is available.';
		return false;
	}

	if (!$RAD10634E7F72CAA071320F21AEE5930D || ($RE5840D3E86DCF8489051E4F70C757552 != '')) {
		$error = 'Error: (' . $server . ') ' . $RE5840D3E86DCF8489051E4F70C757552 . ' (' . $R32D00070D4FFBCCE2FC669BBA812D4C2 . ')';
		return false;
	}

	fputs($RAD10634E7F72CAA071320F21AEE5930D, "$domain\r\n");
	while (!feof($RAD10634E7F72CAA071320F21AEE5930D)) {
		$buffer.= fgets($RAD10634E7F72CAA071320F21AEE5930D, 128);
	}

	fclose($RAD10634E7F72CAA071320F21AEE5930D);
	if ($tld == 'org') nl2br($buffer);
	if ($unknownTLD)	{
		if ((strpos($buffer, 'No match for') !== false) || (strpos($buffer, 'NOT Found') !== false) || (strpos($buffer, 'NOT FOUND') !== false) || (strpos($buffer, 'Not found: ') !== false) || (strpos($buffer, "No Found\n") !== false) || (strpos($buffer, 'NOMATCH') !== false) || (strpos($buffer, "AVAIL\n") !== false) || (strpos($buffer, 'No entries found') !== false) || (strpos($buffer, 'NO MATCH') !== false) || (strpos($buffer, 'No match') !== false) || (strpos($buffer, 'No such Domain') !== false) || (strpos($buffer, 'is free') !== false) || (strpos($buffer, 'FREE') !== false) || (strpos($buffer, 'No data Found') !== false) || (strpos($buffer, 'No Data Found') !== false) || ($buffer == "Available\n") || (strpos($buffer, 'No information about') !== false) || (strpos($buffer, 'no matching record') !== false) || (strpos($buffer, 'does not Exist in database') !== false) || (strpos($buffer, 'Status: AVAILABLE') !== false) || (strpos($buffer, 'not a registered domain') !== false)) {
			return array(
				'true',
				$domainParts
			);
		}

		return false;
	} else {
		if ((strpos($tld, '.au') > 0) && ($buffer == "Not Available\n")) {
			return false;
		}

		if (preg_match('/' . $ext['.' . $tld][1] . '/i', $buffer)) {
			return array(
				'true',
				$domainParts
			);
		} else {
			return false;
		}
	}
	return false;
}
	function getServers() {
		return array(
//	'.EXT' 		=> array('WHOIS SERVER NAME','Text To Match for Available Domain'),
	'.com' 		=> array('whois.crsnic.net','No match for'),
	'.net' 		=> array('whois.crsnic.net','No match for'),	  
	'.org' 		=> array('whois.publicinterestregistry.net','NOT FOUND'),	
	'.us' 		=> array('whois.nic.us','Not Found'),
	'.biz' 		=> array('whois.biz','Not found'),
	'.info' 	=> array('whois.afilias.net','NOT FOUND'),	
	'.mobi'		=> array('whois.dotmobiregistry.net', 'NOT FOUND'),
	'.tv' 		=> array('whois.nic.tv', 'No match for'),
	'.in' 		=> array('whois.inregistry.net', 'NOT FOUND'),
	'.co.uk' 	=> array('whois.nic.uk','No match'),		
	'.co.ug' 	=> array('wawa.eahd.or.ug','No entries found'),	
	'.or.ug' 	=> array('wawa.eahd.or.ug','No entries found'),
	'.sg' 		=> array('whois.nic.net.sg','Domain Not Found'),	
	'.com.sg' 	=> array('whois.nic.net.sg','Domain Not Found'),	
	'.per.sg' 	=> array('whois.nic.net.sg','Domain Not Found'),	
	'.org.sg' 	=> array('whois.nic.net.sg','Domain Not Found'),	
	'.com.my' 	=> array('whois.mynic.net.my','does not Exist in database'),	
	'.net.my' 	=> array('whois.mynic.net.my','does not Exist in database'),	
	'.org.my' 	=> array('whois.mynic.net.my','does not Exist in database'),	
	'.edu.my' 	=> array('whois.mynic.net.my','does not Exist in database'),	
	'.my' 		=> array('whois.mynic.net.my','does not Exist in database'),		
	'.nl' 		=> array('whois.domain-registry.nl','not a registered domain'),
	'.ro' 		=> array('whois.rotld.ro','No entries found for the selected'),
	'.com.au'	=> array('whois-check.ausregistry.net.au',"Available\n"),
	'.net.au'	=> array('whois-check.ausregistry.net.au',"Available\n"),
	'.ca' 		=> array('whois.cira.ca', 'AVAIL'),
	'.org.uk'	=> array('whois.nic.uk','No match'),
	'.name' 	=> array('whois.nic.name','No match'),
	'.ac.ug' 	=> array('wawa.eahd.or.ug','No entries found'),
	'.ne.ug' 	=> array('wawa.eahd.or.ug','No entries found'),
	'.sc.ug' 	=> array('wawa.eahd.or.ug','No entries found'),
	'.ws'		=> array('whois.website.ws','No Match'),
	'.be' 		=> array('whois.ripe.net','No entries'),
	'.com.cn' 	=> array('whois.cnnic.cn','no matching record'),
	'.net.cn' 	=> array('whois.cnnic.cn','no matching record'),
	'.org.cn' 	=> array('whois.cnnic.cn','no matching record'),
	'.no'		=> array('whois.norid.no','no matches'),
	'.se' 		=> array('whois.nic-se.se','No data found'),
	'.nu' 		=> array('whois.nic.nu','NO MATCH for'),
	'.com.tw' 	=> array('whois.twnic.net','No such Domain Name'),
	'.net.tw' 	=> array('whois.twnic.net','No such Domain Name'),
	'.org.tw' 	=> array('whois.twnic.net','No such Domain Name'),
	'.cc' 		=> array('whois.nic.cc','No match'),
	'.nl' 		=> array('whois.domain-registry.nl','is free'),
	'.pl' 		=> array('whois.dns.pl','No information about'),
	'.eu' 		=> array('whois.eu','Status:	AVAILABLE'),
	'.pt' 		=> array('whois.dns.pt','No match'),
	'.com.na' 		=> array('whois.na-nic.com.na','Not registered'),
	'.na' 		=> array('whois.na-nic.com.na','Not registered'),
	'.de' 		=> array('whois.denic.de','Status: free'),
	'.at' 		=> array('whois.nic.at','% nothing found'),	
	'.ch' 		=> array('whois.nic.ch','do not have an entry in our database matching'),	
	'.li' 		=> array('whois.nic.ch','do not have an entry in our database matching'),	
	'.se' 		=> array('whois.iis.se','not found') 
);
}
?>
