var fs = require('fs');
var allWords = [];

module.exports = function (input) {
	var words;
	if (allWords.length == 0) { //Load
		console.log('Loading wordlist');
		var words = fs.readFileSync("words.txt","utf8").split('\n').filter(function(word) {
			return word.length >= 4;
		});
		allWords = words;
	} else {
		words = allWords;
	}
	if (words === null) words = allWords;
	var output=[];
	words.forEach( function(word) {
		if (input.match(word)) output.push(word);
	});
	return output;
}
