#!/usr/bin/node
// Downloads pools data, and imports records into a MongoDB Que
var progress = require('progress');
var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://worker:3v3rm0r3@localhost:27017/domainomatics';
var dbBar,subCount;
var progress = require('progress');

function removeUnverified(collection,cb) {
	console.log('Removing unverified alerts');
	var threshold = Math.floor(new Date() / 1000);
	threshold = threshold - (24 * 60 * 60); //Older than a day
	collection.remove({created:{$lt:threshold},verification:''}, function(err,result) {
		if (err) {
			cb(err);
		} else {
			cb(null,result);
		}
	});
}

function initAlerts(collection) {
	console.log('Enumerating records.');
	var count = collection.find({}).count(function(err,count) {
		dbBar = new progress('Running ALerts [:bar] :percent :current/:total (:elapsed/:eta)',{
			complete: '=',
			incomplete: ' ',
			width:100,
			total:count
		});
		collection.find({},function(err,docs) {
			docs.each(function(err,doc) {
				//email, target, newsletter
				if (err) {
					console.log('Error :'+err);
					process.exit(1); //Bail
				} else {
					subCount++;
					dbBar.tick();
					if (doc.words === undefined) {
						var wordList=words(doc.text.sld); //Search for words
						//Fire email triggers for each subscribed word.
						collection.updateOne({"_id":doc._id},{$set:{"words":wordList}});
						if (subCount == count) { //Done
							process.exit(0);
						}
					}
				}
			});
		});
	});
}



MongoClient.connect(url,function(err,db) {
	if (err) {
		console.log('Connection Error: '+err);
		process.exit(1);
	}
	var collection = db.collection('alerts');
	removeUnverified(collection, function(err,result) {
		if (err) {
			console.log('Error :'+err);
			process.exit();
		}
		initAlerts(collection);
	});
});
