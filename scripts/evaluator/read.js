// Downloads pools data, and imports records into a MongoDB Que
var dataStore = require('nedb');
var nedb = new dataStore({ filename: 'domains.db', autoload:true});
var progress = require('progress');
var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://worker:3v3rm0r3@localhost:27017/domainomatics';
var maxRecords = 30000;

function injectResults(db) {
	var objCount = 0;
	var collection = db.collection('domains');
	console.log('Sorting records.');
	nedb.find({}).sort({tldFactor:-1,letterFactor:-1,vowelFactor:-1,sylFactor:-1}).limit(maxRecords).exec(function(err,docs) {
		if (err) {
			console.log('Sorting Error: '+err);
			process.exit();
		}
		var dbBar = new progress('Injecting Records [:bar] :percent :current/:total (:elapsed/:eta)',{
			complete: '=',
			incomplete: ' ',
			width:100,
			total:maxRecords
		});
		
		docs.forEach( function(doc) {
			collection.insert(doc, function(err,result) {
				if (err) {
					console.log('Insert Error: '+err);
					process.exit();
				}
				objCount++;
				dbBar.tick();
				if (objCount == maxRecords) {
					console.log('Injection completed.');
					process.exit();
				}
			});
		});
	});
}

MongoClient.connect(url,function(err,db) {
	if (err) {
		console.log('Connection Error: '+err);
		process.exit();
	}
	var collection = db.collection('domains');
	console.log('Removing old data');
	console.log('Ensuring Indexes');
	collection.createIndex( { "words": 1 } );
	collection.createIndex( { "expires": 1 }, { expireAfterSeconds: ( 60 * 60 * 24 * 5 ) } );
	injectResults(db);
});
