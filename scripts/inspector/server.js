#!/usr/bin/node
var dd=require('./index.js');
var http = require('http');
var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://worker:3v3rm0r3@localhost:27017/domainomatics';
var validator = require('validator');
var mdb;

const PORT=8080;

function handleRequest(request,response) {
	var id = request.url.substring(1);
	var collection = mdb.collection('domains');
	collection.find({_id:id}).toArray(function(err,results) {
		var result = results.pop();
		if (err) {
			console.log('Error: '+err);
		}
		var domain = result.domain;
		if (validator.isFQDN(domain)) {
			try {
				console.time(domain);
				dd.detective(domain,function(data) {
					console.timeEnd(domain);
					result.extended = data;
					collection.update({_id:id},result,function() {
						response.end(JSON.stringify(data));
					});
				});
			} catch (e) {
				console.log('Detector Error: '+e);
			}
		} else {
			console.log('Invalid id : '+id);
			response.end('Invalid id');
		}
	});
}

var server = http.createServer(handleRequest);

MongoClient.connect(url,function(err,db) {
	mdb = db; //Carry to a bigger scope
	console.log('Database Connected.');
        if (err) {
                console.log('Connection Error: '+err);
                process.exit();
	}
	server.listen(PORT, function() {
		console.log('Server is now listening on port '+PORT);
	});
});
