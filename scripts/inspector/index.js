var async=require('async');
var ip=require('ip');
var uni = require('unirest');
var cheerio = require('cheerio');
var pageRank = require('pagerank');
var domainParser = require('domain-name-parser');
var timeout=1000; //MS of max time to wait for rest requeset
var domain='';
var domainData={};
var myIP = ip.address();
var debugLevel=0;

function validIP(ipaddress) {  //Used for IP finding and GEO
	if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress))  {  
		return (true)  
	}  
	return (false)  
}  

function textAnalyze(callback) {
	domainData.text = {};

	if (debugLevel > 1) console.log('Text analysis : Initiated.');
	var d = domainParser(domain);

	domainData.text.tld = d.tld;
	domainData.text.sld = d.sld;
	//Does domain contain hyphens?
	domainData.text.hyphen = Boolean(/[\-]/.test(d.sld));
	domainData.text.numeric = Boolean(/[0-9]/.test(d.sld));
	
	word = d.sld.toLowerCase();
	if(word.length <= 3) domainData.text.syllables = 1; 
	word = word.replace(/(?:[^laeiouy]es|ed|[^laeiouy]e)$/, ''); 
	word = word.replace(/^y/, '');
	try {
		domainData.text.syllables = word.match(/[aeiouy]{1,2}/g).length;
	} catch(e) {
		domainData.text.syllables = 0;
	}

	domainData.text.domainLength = d.sld.length;

	if (debugLevel > 1) console.log('Text analysis : Completed.');
	callback(null,true);
}

function googleCount(callback) {
	if (debugLevel > 1) console.log('Google Count : Initiated.');
	uni.get('http://ajax.googleapis.com/ajax/services/search/web?v=1.0&filter=0&q=site:'+domain).timeout(timeout).end(function(response) {
		if (debugLevel > 1) console.log('Google Count : Completed.');
		try {
			var data = JSON.parse(response.body);
			if (data.responseData) {
				domainData.googleCount = data.responseData.cursor.estimatedResultCount;
				callback(null,true);
			} else {
				domainData.googleCount = 0;
				callback(null,true);
			}
		} catch (e) {
			callback("GoogleCount "+e,false);
			return;
		}
	});
}

function bingCount(callback) {
	if (debugLevel > 1) console.log('Bing Count : Initiated.');
	uni.get('http://www.bing.com/search?q=site:'+domain+'&FORM=QBRE&mkt=en-US').timeout(timeout).end( function(response) {
		if (debugLevel > 1) console.log('Bing Count : Completed.');
		$ = cheerio.load(response.body);
		domainData.bingCount = $('span[class=sb_count]').text().replace(/\D/g,'');
		callback(null,true);
	});
}

function yahooCount(callback) {
	if (debugLevel > 1) console.log('Yahoo Count : Initiated.');
	uni.get('http://search.yahoo.com/search;_ylt=?p=site:'+domain).timeout(timeout).end(function(response) {
		if (debugLevel > 1) console.log('Yahoo Count : Completed.');
		var $ = cheerio.load(response.body);
		domainData.yahooCount = $('span','div[class=compPagination]').text().replace(/\D/g,'');
		callback(null,true);
	});
}

function googlePROld(callback) { //Goes over quota often
	if (debugLevel > 1) console.log('Google PageRank : Initiated');
	uni.get('http://www.prapi.net/pr.php?url=http://'+domain+'&f=json').timeout(timeout).end(function(response) {
		if (debugLevel > 1) console.log('Google PageRank : Completed.');
		try {
			domainData.googlePR = response.body.pagerank;
			callback(null,true);
		} catch (e) {
			callback("GooglePR "+e,false);
			return;
		}
	});
}

function googlePR(callback) { //Goes over quota often
	if (debugLevel > 1) console.log('Google PageRank : Initiated');

	new pageRank('http://'+domain, function(error,pr) {
		if (debugLevel > 1) console.log('Google PageRank : Completed.');
		if (error) {
			callback(error,null);
		} else {
			domainData.googlePR = pr;
			callback(null,true);
		}
	});
}

function dmozListing(callback) {
	if (debugLevel > 1) console.log('DMOZ Listing : Initiated');
	uni.get('http://dmoz.org/search/?q='+encodeURIComponent(domain)).timeout(timeout).end(function(response) {
		if (debugLevel > 1) console.log('DMOZ Listing : Completed.');
		if (response.body.indexOf('DMOZ Sites') == -1) {
			domainData.dmozListing = false;
		} else {
			domainData.dmozListing = true;
		}
		callback(null,true);
	});
}

function alexaRankBL(callback) {
	if (typeof domainData.alexa === 'undefined') domainData.alexa = {};	
	if (debugLevel > 1) console.log('Alexa Ranking : Initiated');
	uni.get('http://data.alexa.com/data?cli=10&dat=snbamz&url='+domain).timeout(timeout).end( function(response) {
		if (debugLevel > 1) console.log('Alexa Ranking : Completed.');
		var $ = cheerio.load(response.body,{xmlMode:true});
		if ($('LINKSIN') !== undefined) { //Alexa Backlinks
			domainData.alexa.Rank = $('LINKSIN').attr('NUM');
		} else { //NoBackLinks
			domainData.alexa.Backlinks = 0;
		}
		domainData.alexa.Rank = $('POPULARITY').attr('TEXT');
		callback(null,true);
	});
}

function siteSpeed(callback) {
	if (debugLevel > 1) console.log('Site Speed : Initiated');
	uni.get('https://www.googleapis.com/pagespeedonline/v1/runPagespeed?userIp='+myIP+'&fields=score&url=http://'+domain).timeout(timeout).end(function(response) {
		if (debugLevel > 1) console.log('Site Speed : Completed.');
		try {
			domainData.siteSpeed = response.body.score;
			callback(null,true);
		} catch(e) {
			callback("SiteSpeed "+e,false);	
			return;
		}
	});
}

function contentBreakdown(callback) {
	if (debugLevel > 1) console.log('Content Breakdown : Initiated');
	uni.get('https://www.googleapis.com/pagespeedonline/v1/runPagespeed?userIp='+myIP+'&url=http://'+domain).timeout(timeout).end(function(response) {
		if (debugLevel > 1) console.log('Content Breakdown : Completed.');
		try {
			domainData.contentBreakdown = response.body.pageStats;
			callback(null,true);
		} catch(e) {
			callback("ContentBreakdown "+e,null);
			return;
		}
	});
}

function alexaBR(callback) {
	if (typeof domainData.alexa === 'undefined') domainData.alexa = {};	
	if (debugLevel > 1) console.log('Alexa BounceRate : Initiated');
	uni.get('http://www.alexa.com/siteinfo/'+domain).timeout(timeout).end(function(response){
		if (debugLevel > 1) console.log('Alexa BounceRate : Completed.');
		try {
			var $ = cheerio.load(response.body);
			$('strong.metrics-data').each(function(i, element){
				var a = $(this).text().trim();
				if (i==1) domainData.alexa.BounceRate = a;
				if (i==2) domainData.alexa.ViewsPerVisitor = a;
				if (i==3) domainData.alexa.TimeOnSite = a;
				if (i==4) domainData.alexa.SearchVisits = a;
			});
			callback(null,true);
		} catch (e) {
			callback("Alexa "+e,null);
			return;
		}
	});
}

function twitterShares(callback) {
	if (debugLevel > 1) console.log('Twitter Shares : Initiated');
	uni.get('http://urls.api.twitter.com/1/urls/count.json?url='+domain).timeout(timeout).end(function(response) {
		if (debugLevel > 1) console.log('Twitter Shares : Completed.');
		try {
			domainData.twitterShares = response.body.count;
			callback(null,true);
		} catch(e) {
			callback("TwitterShares "+e,null);
			return;
		}
	});
}

function linkedInShares(callback) {
	if (debugLevel > 1) console.log('LinkedIn Shares : Initiated');
	uni.get('https://www.linkedin.com/countserv/count/share?url='+domain+'&format=json').timeout(timeout).end(function(response) {
		if (debugLevel > 1) console.log('LinkedIn Shares : Completed.');
		try {
			domainData.LinkedInShares = response.body.count;
			callback(null,true);
		} catch(e) {
			callback("LinkedInShares "+e,null);
			return;
		}
	});
}

function fbStats(callback) {
	if (debugLevel > 1) console.log('Facebook Stats : Initiated');
	uni.get('http://api.facebook.com/restserver.php?method=links.getStats&urls=http://'+domain+'&format=json').timeout(timeout).end(function(response){
		if (debugLevel > 1) console.log('Facebook Stats : Completed.');
		try {
			domainData.fbStats = response.body[0];
			callback(null,true);
		} catch (e) {
			callback("FBStats "+e,null);
		}
	});
}

function googlePlus(callback) {
	if (debugLevel > 1) console.log('Google Plus : Initiated');
	uni.get('https://plusone.google.com/_/+1/fastbutton?url=http://'+domain+'/').timeout(timeout).end(function(response) {
		if (debugLevel > 1) console.log('Google Plus : Completed.');
		try {
			var $ = cheerio.load(response.body);
			domainData.googlePlus = $('div#aggregateCount').text();
			callback(null,true);
		} catch (e) {
			callback("GooglePlus "+e,null);
		}
	});
}

function stumbleUpon(callback) {
	if (debugLevel > 1) console.log('Stumble Upon : Initiated');
	uni.get('http://www.stumbleupon.com/services/1.01/badge.getinfo?url='+domain).timeout(timeout).end(function(response) {
		if (debugLevel > 1) console.log('Stumble Upon : Completed.');
		try {
			var views = 0;
			if (response.body.result !== undefined) {
				if (response.body.result.views !== undefined) {
					views = response.body.result.views;
				}
			}
			domainData.stumbleUpon = views;
			callback(null,true);
		} catch (e) {
			callback("StumbleUpon "+e,null);
		}
	});
}

function pinterest(callback) {
	if (debugLevel > 1) console.log('Pinterest : Initiated');
	uni.get('http://api.pinterest.com/v1/urls/count.json?url=http://'+domain).timeout(timeout).end(function(response) {
		if (debugLevel > 1) console.log('Pinterest : Completed.');
		try {
			var regExp = /\(([^)]+)\)/;
			var matches = (regExp.exec(response.body));
			var data = JSON.parse(matches[1]);
			domainData.pinterest = data.count; 
			callback(null,true);
		} catch(e) {
			callback("Pinterest "+e,null);
		}
	});
}

function vkShares(callback) {
	if (debugLevel > 1) console.log('VK Shares : Initiated');
	uni.get('http://vk.com/share.php?act=count&index=1&url=http://'+domain).timeout(timeout).end(function(response) {
		if (debugLevel > 1) console.log('VKShares : Completed.');
		try {
			var regExp = /\(([^)]+)\)/;
			var matches = (regExp.exec(response.body));
			if (matches) {
				var counts = matches[1].split(',');
				domainData.vkShares = counts[1];
				callback(null,true);
			} else {
				domainData.vkShares = 0;
				callback(null,true);
			}
		} catch(e) {
			callback("vkShares "+e,null);
		}
	});
}

function safeBrowsing(callback) {
	if (debugLevel > 1) console.log('Safe Browsing : Initiated');
	uni.get('http://www.google.com/safebrowsing/diagnostic?site='+domain).timeout(timeout).end(function(response) {
		if (debugLevel > 1) console.log('Safe Browsing : Completed.');
		try {
			if (response.body.indexOf('This site is not currently listed as suspicious') == -1) {
				domainData.safeBrowsing = true;
			} else {
				domainData.safeBrowsing = false;
			}
			callback(null,true);
		} catch(e) {
			callback("SafeBrowsing "+e,null);
		}
	});
}

function spamHaus(callback) {
	if (debugLevel > 1) console.log('SpamHaus : Initiated');
	uni.get('https://www.spamhaus.org/query/domain/'+domain).timeout(timeout).end(function(response) {
		if (debugLevel > 1) console.log('SpamHaus : Completed.');
		try {
			if (response.body.indexOf('is not listed in the DBL') == -1) {
				domainData.spamHaus = true;
			} else {
				domainData.spamHaus = false;
			}
			callback(null,true);
		} catch(e) {
			callback("SpamHaus "+e,null);
		}
	});
}

function domainAuthority(callback) { //Requires API Key, decommissioned
	if (debugLevel > 1) console.log('Domain Authority : Initiated');
	uni.get('http://www.spamhaus.org/query/domain/'+domain).timeout(timeout).end(function(response) {
		if (debugLevel > 1) console.log('Domain Authority : Completed.');
		try {
			if (response.body.indexOf('is not listed in the DBL') == -1) {
				domainData.spamHaus = false;
			} else {
				domainData.spamHaus = true;
			}
			callback(null,true);
		} catch(e) {
			callback("DomainAuthority "+e,null);
		}
	});
}


function whois(callback) {
	if (debugLevel > 1) console.log('Whois : Initiated');
	uni.get('http://whois.websitevaluecalculatorscript.com/?url=http://'+domain+'&format=json').timeout(timeout).end(function(response){
		if (debugLevel > 1) console.log('Whois : Completed.');
		try {
			domainData.whois = response.body;
			callback(null,true);
		} catch (e) {
			callback("Whois "+e,null);
		}
	});
}

function geo(callback) {
	if (debugLevel > 1) console.log('GeoLocation IPs: Initiated');
	uni.get('https://dnshistory.org/historical-dns-records/a/'+domain).timeout(timeout).end(function(response){
		if (debugLevel > 1) console.log('GeoLocation IPs: Completed.');
		try {
			var ips=[];
			var $ = cheerio.load(response.body);
			$('a','div#mainarea').each( function(i,element) {
				var cur = $(this).text().trim();
				if (ips.latest != undefined) ips.push(cur);
				ips.latest = cur;
			});
			if (validIP(ips.latest)) { //Historic IPs found
				domainData.ips = ips;
				if (debugLevel > 1) console.log('GeoLocation Query : Initiated');
				uni.get('http://freegeoip.net/json/'+ips.latest).timeout(timeout).end(function(response) {
					if (debugLevel > 1) console.log('GeoLocation Query : Completed.');
					domainData.geo = response.body;
					callback(null,true);
				});
			} else { //No historic IP found
				domainData.ips = null;
				domainData.geo = null;
				callback(null,true);
			}
		} catch (e) {
			callback("Geo "+e,null);
		}
	});
}

function detective(targetDomain,callback) {
	var tasks=[];	
	domain=targetDomain;

	domainData.domain = domain;
	domainData.created = Math.floor(new Date() / 1000);

	if (debugLevel > 1) console.log('Initiated detective for : '+domain);
	if (debugLevel > 1) console.log('Detected Local IP : '+myIP);

	//Que Detective Tasks

	//Text Analytics
	tasks.push( textAnalyze );
	
	//GeoLocation + IPs
	tasks.push( geo );

	//SpeedScore - Took too much time, removed 1/5/16 
	//tasks.push( siteSpeed );
	
	//Google Counts
	tasks.push( googleCount );

	//Bing Counts
	tasks.push( bingCount );

	//Yahoo Counts
	tasks.push( yahooCount );

	//GooglePagerank -- fails too much
	//tasks.push( googlePR );

	//Dmoz Listing
	tasks.push( dmozListing );
	
	//Alexa Rank & Back Links
	tasks.push( alexaRankBL );
	
	//ContentBreakdown - no good for expired domains
	//tasks.push( contentBreakdown );

	//AlexaBounceRate
	tasks.push( alexaBR );

	//Twitter Shares -- Removed 1/4/16
	//tasks.push( twitterShares );
	
	//Linkedin Shares
	tasks.push( linkedInShares );

	//Facebook Stats
	tasks.push( fbStats );

	//GooglePlus One
	tasks.push( googlePlus );

	//StumbleUpon
	tasks.push( stumbleUpon );

	//Pinterest
	tasks.push( pinterest );
	
	//VKShares 
	tasks.push( vkShares );

	//SafeBrowsing 
	tasks.push( safeBrowsing );

	//SpamHaus -- removed 1/4/16 due to updates
	//tasks.push( spamHaus );	

	//GooglePR
	tasks.push( googlePR );

	//Whois - Junk for now
	//tasks.push( whois );

	if (debugLevel > 1) console.log('Task Que Size : '+tasks.length);

	async.parallel(
		tasks, 
		function(err,results) {
			if (err) {
				console.log("Error "+err);
			}
			if (debugLevel > 1) console.log('All tasks completed.');
			if ((debugLevel > 0) && (err != null)) { console.log(err); }
			callback(domainData);
		}
	);
}

module.exports = {
	detective:detective
}
