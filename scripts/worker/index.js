var url = 'mongodb://worker:3v3rm0r3@104.128.238.243:27017/domainomatics';
var monq = require('monq');
var client = monq( url );
var worker = client.worker(['queue'],{interval:1000});
var inspector = require('../inspector');

var MongoClient = require('mongodb').MongoClient;

MongoClient.connect(url, function(err,db) {
	if (err) {
		console.log('Error DB '+err);
		process.exit();
	}

	var collection = db.collection('domains');
	worker.register({
		pool: function(params,callback) {
			try {
				inspector.detective(params.domain,function(data) {
					if (err) {
						console.log('Error '+err);
						callback(err);
					} else {
						var theNow = new Date();
						collection.insert({'domain':params.domain,'priority':params.priority,'data':data,'expires':params.expires,'created':theNow});
						console.log(params.domain);
						callback(null,true);
					}
				});
			} catch (err) {
				callback(err);
			}
		}	
	});

	
	worker.on('error',function(err) {
		console.log('Error '+err);
	});
	
	worker.on('completed', function(data) {
		console.log('Completed');
	});
	worker.on('dequeue', function(data) {
		console.log(params.domain);	
	});

	worker.start();
});
