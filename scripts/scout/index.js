// Downloads pools data, and imports records into a MongoDB Que
var monq = require('monq');
var client = monq('mongodb://worker:3v3rm0r3@localhost:27017/domainomatics');
var que = client.queue('queue');

var domainParser = require('domain-name-parser');

var args = require('minimist')(process.argv.slice(2));

var MongoClient = require('mongodb').MongoClient;

var bufferJoiner = require('bufferjoiner');
var csv = require('fast-csv');
var http = require('http');
var fs = require('fs');
var admZip = require('adm-zip');

function textAnalyze(domain) {
	var text = {};
        var d = domainParser(domain);
        text.tld = d.tld;
        text.sld = d.sld;
        text.hyphen = Boolean(/[\-]/.test(d.sld));
        text.numeric = Boolean(/[0-9]/.test(d.sld));
        word = d.sld.toLowerCase();
        if(word.length <= 3) text.syllables = 1;
        word = word.replace(/(?:[^laeiouy]es|ed|[^laeiouy]e)$/, '');
        word = word.replace(/^y/, '');
        try {
        	text.syllables = word.match(/[aeiouy]{1,2}/g).length;
        } catch(e) {
        	text.syllables = null;
        }
        text.domainLength = d.sld.length;

	return text;
}

function letterScore(word) { //Use Scrable scores
	var score = 0;
	var i = word.length;
	while (i--) {
		var c = word[i];
		if (c == 'a') score += 1;
		if (c == 'b') score += 3;
		if (c == 'c') score += 3;
		if (c == 'd') score += 2;
		if (c == 'e') score += 1;
		if (c == 'f') score += 4;
		if (c == 'g') score += 2;
		if (c == 'h') score += 4;
		if (c == 'i') score += 1;
		if (c == 'j') score += 8;
		if (c == 'k') score += 5;
		if (c == 'l') score += 1;
		if (c == 'm') score += 3;
		if (c == 'n') score += 1;
		if (c == 'o') score += 1;
		if (c == 'p') score += 3;
		if (c == 'q') score += 10;
		if (c == 'r') score += 1;
		if (c == 's') score += 1;
		if (c == 't') score += 1;
		if (c == 'u') score += 1;
		if (c == 'v') score += 4;
		if (c == 'w') score += 4;
		if (c == 'x') score += 8;
		if (c == 'y') score += 4;
		if (c == 'z') score += 10;
		if (c == '-') score += 10;
		if (c == '0') score += 10;
		if (c == '1') score += 10;
		if (c == '2') score += 10;
		if (c == '3') score += 10;
		if (c == '4') score += 10;
		if (c == '5') score += 10;
		if (c == '6') score += 10;
		if (c == '7') score += 10;
		if (c == '8') score += 10;
		if (c == '9') score += 10;
	}
	return score;
}

function getPriority(domain) {
	var text = textAnalyze(domain);
	var score = 0;
	var mult = 1;
	
	//Extention multipliers
	if (text.tld == 'com') mult = 3;
	if (text.tld == 'net') mult = 1.6;
	if (text.tld == 'org') mult = 1.3;
	if (text.tld == 'edu') mult = 1.2;
	if (text.tld == 'gov') mult = 1.2;
	if (text.tld == 'info') mult = 1.1;
	if (text.tld == 'us') mult = 1.1;
	if (text.tld == 'co.uk') mult = 1.1;
	
	//Uncommon characters
	var lScore = letterScore(domain);
	score = ( 1 / lScore ) * 100;

	//Syllables
	if (text.syllables) {
		if (text.syllables == 1) mult += 1;
		if (text.syllables == 2) mult += 0.5;
		if (text.syllables == 3) mult + 0.25 ;
	}

	return (score * mult);
	//Use text data to create numeric priority
}

function parseExpires(expires) {
	var dateParts = expires.split(/[\/ ]/);
	return new Date(dateParts[2],dateParts[0],dateParts[1]);
}

//Function to initiate streams
function downloadPool(url,callback) {
	var data = [];
	var dataLen = 0;
	var bf = bufferJoiner();
	var request = http.get("http://www.pool.com/Downloads/PoolDeletingDomainsList.zip",function(response) {
		console.log('Download Initiated.');
		response.on("data", function(chunk) {
			bf.add(chunk);
			data.push(chunk);
			dataLen += chunk.length;
		});
		response.on("end",function() {
			console.log("Download Completed.");
			console.log("Parsing file.");
			var buf = new Buffer(dataLen);
			buf = bf.join(true);
			var zip = new admZip(buf);
			var zipEntries = zip.getEntries();
			var dCount = 0;
			csv
				.fromString( zip.readAsText(zipEntries[0]) )
				.on("data", function(data) {
					var priority = getPriority(data[0]);					
					var expires = parseExpires(data[1]);
					var obj = {
						domain:data[0],
						expires:expires,
						priority:priority
					};
					que.enqueue( 'pool' , obj, {priority:priority}, function (err,job) {
						if (err) {
							console.log("ERROR "+err);
						} else {
							dCount++;
						}
					});

				})
				.on("end", function() {
					console.log("Enqueued "+dCount+" expiring domains.");
					process.exit();
				});
			
		});
	});
}

function clean() {
	MongoClient.connect('mongodb://127.0.0.1:27017/domainomatics',function (err,db) {
		if (err) {
			console.log('Error '+err);
			process.exit();
		}
		var threshold = new Date();
		db.collection('jobs').remove();
		console.log('All jobs removed.');
	});
}

if (args['clean']) {
	clean();
}

if (args['download']) {
	downloadPool('http://www.pool.com/Downloads/PoolDeletingDomainsList.zip');
}
