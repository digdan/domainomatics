var fs = require('fs');
module.exports = function (vars ,cb) {
	fs.readFile(__dirname+"/emailtemplate.htm",'utf8',function(err,str) {
		if (err) {
			return console.log(err);
		}
		for (var key in vars) {
			str = str.replace( key.toUpperCase(), vars[key] );
		}
		cb(str);
	});
};
