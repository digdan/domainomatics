var url = 'mongodb://worker:3v3rm0r3@104.128.238.243:27017/domainomatics';
var templated = require('./templated.js');
var MongoClient = require('mongodb').MongoClient;
var sendgrid = require("sendgrid").SendGrid("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");
var helper = require("sendgrid").mail;

var findTriggers = function(db, callback) {
	var docs = [];
	var cursor = db.collection('alerts').find();
	cursor.each(function(err,doc) {
		docs.push(doc);
		if (doc == null) callback(docs);
	});
}

var processDoc = function(db,doc,cb) {
	var vars=[];
	if (doc !== null) {
		var footer = '<A href="http://www.domainomatics.com/unsubscribe/'+doc.verification+'"> Click here to unsubscribe </A>';
		var cursor = db.collection('domains').find({'text.sld':{'$regex':doc.target}});
		cursor.count(function(err,count) {
			var content = "<p>You are subscribed to domainomatics alert system. We have queried domains for your keyword '"+doc.target.toUpperCase()+"' that will expire soon. </p>";
			content = content + "<BR>\n<div style='text-align:center'> <B>"+doc.target.toUpperCase()+"</B> has "+count+" results</div><BR>\n";
			content = content + "<table style='width:100%'><tr><th><b> Domain Name </b></th><th><b> Expires </b></th></tr>";	
			var iter = cursor.limit(20).sort({"letterFactor":1});
			iter.each(function(err,cur) {
				if (cur == null) {
					content = content + "</table><BR>\n";
					var action = "<div style='text-align:center'><A HREF='http://www.domainomatics.com/search/"+doc.target+"'> View all "+count+" results here</a></div>\n";
					vars['content'] = content;
					vars['footer'] = footer;
					vars['action'] = action;
					cb(vars);
				} else {					
					var expireParts = cur.expires.toString().split(" ");
					var expires = expireParts[1]+" "+expireParts[2]+" "+expireParts[3];
					content = content + "<tr><td><a href='http://www.domainomatics.com/lookup/"+cur.domain+"'> "+cur.domain+" </a> </td><td> "+expires+"</td></tr>\n";
				}
			});
		});
	}
}

MongoClient.connect(url, function(err,db) {
	if (err) {
		console.log('Error DB '+err);
		process.exit();
	}

	findTriggers(db,function(docs) {
		docs.forEach(function(doc) { //Each trigger
			processDoc(db,doc, function(vars) {
				templated( vars ,function(template) {
					var email = new helper.Mail(
						new helper.Email("info@domainomatics.com"),
						'Domainomatics Alert',
						new helper.Email( doc.email ),
						new helper.Content("text/html",template)
					);
					var request = sendgrid.emptyRequest();
					request.method='POST';
					request.path = '/v3/mail/send';
					request.body = email.toJSON();
					sendgrid.API(request,function(response) {
						console.log('Processed "'+doc.target+'" for '+doc.email+"\n");
					});
				});
			});
		});
		setTimeout(function() {
			process.exit();
		}, 1500);
	});
});
