<?php

// Kickstart the framework
$f3=require('lib/base.php');

$f3->set('DEBUG',1);
if ((float)PCRE_VERSION<7.9)
	trigger_error('PCRE version is out of date');

// Load configuration
$f3->config('.config.ini');
include('app/predis.php'); //Redis client
include('app/db.php');
include('app/status.php');
include('app/scores.php');
include('app/pagination.php');
include('app/meta.php');
$pageLimit = 10;

//Set Meta Tags
$f3->set('meta',array(
	'title'=>appMeta::title(),
));


//Build Route Definitions
$f3->route('GET /',
	function($f3) {
		global $pageLimit;
		$domains = appDB::getDomains();
		$pages = appDB::getPages();
		$page = 1;
		$pagination = new Pagination($pageLimit,$pages,$page,'/page/');
		$f3->set('domainCount',number_format(appDB::getCount()));
		$f3->set('pagination',$pagination->pagination(true));
		$f3->set('domains',$domains);
		$f3->set('content','welcome.htm');
		echo View::instance()->render('layout.htm');
	}
);
$f3->route('GET /ac/@term',
	function($f3) {
		echo json_encode( appDB::getAC($f3->get("PARAMS.term")) );
		die();
	}
);
$f3->route('POST /search',
	function($f3) {
		$term = $_POST['search'];
		header("Location: /search/".$term);
		die();
	}
);
$f3->route('GET /search/@term',
	function($f3) {
		global $pageLimit;
		$term = $f3->get("PARAMS.term");
		$searchInfo = appDB::getSearch($term);
		$domains = $searchInfo['list'];
		$pages = floor( $searchInfo['total'] / appDB::$limit );
		$page = 1;
		$pagination = new Pagination($pageLimit,$pages,$page,'/page/');
		$f3->set('term',$term);
		$f3->set('pagination',$pagination->pagination(true));
		$f3->set('domains',$domains);
		$f3->set('content','welcome.htm');
		echo View::instance()->render('layout.htm');
		
	}
);
$f3->route('GET /page/@page',
	function($f3) {
		global $pageLimit;
		$page = $f3->get("PARAMS.page");
		$domains = appDB::getDomains($page);
		$pages = appDB::getPages();
		$pagination = new Pagination($pageLimit,$pages,$page,'/page/');
		$f3->set('pagination',$pagination->pagination(true));
		$f3->set('domains',$domains);
		$f3->set('content','welcome.htm');
		echo View::instance()->render('layout.htm');
	}
);

$f3->route('GET /lookup/@domain',
	function($f3) {
		$domain = $f3->get("PARAMS.domain");
		if ($domainData = appDB::getDomain($domain)) {
			$f3->set('domain',$domain);
			$f3->set('domainData',$domainData);	
			$f3->set('content','domain.htm');
			echo View::instance()->render('layout.htm');
		} else {
			header("HTTP/1.0 404 Not Found");
			$f3->set('content','notfound.htm');
			echo View::instance()->render('layout.htm');
		}
	}
);

$f3->route('GET /status/@domain',
	function($f3) {
		$domain = $f3->get("PARAMS.domain");
		if ($domainData = appDB::getDomain($domain)) {
			$status = appDB::getStatus($domainData['_id']);
			if ($status == false) {
				$status = appStatus::getStatus($domain);
				if ($status->isRegistered == true) { // Remove from list
					appDB::removeDomain($domain);
				} else {
					appDB::saveStatus($status,$domainData['_id']);
				}
			}
			echo json_encode($status);
			die();
		} else {
			header("HTTP/1.0 404 Not Found");
			$f3->set('content','notfound.htm');
			echo View::instance()->render('layout.htm');
		}
	}
);
$f3->route('GET /extended/@id',
	function($f3) {
		$id = $f3->get('PARAMS.id');
		if ($listing = appDB::getId($id)) {
			$f3->set('list',$listing);
			if (!$listing['extended']) {
				file_get_contents('http://localhost:8080/'.$id);
				$listing = appDB::getId($id);
			}
			$f3->set('extended',$listing['extended']);
			$f3->set('content','extended.htm');
			echo View::instance()->render('partial.htm');
		} else {
			$f3->set('content','notfound.htm');
			echo View::instance()->render('partial.htm');
		}
	}
);

$f3->route('GET /alerts',
	function($f3) {
		$f3->set('content','alerts.htm');
		echo View::instance()->render('layout.htm');
	});
$f3->route('POST /alerts',
	function($f3) {
		$f3->set('content','alert-thanks.htm');
		// if email valid
		if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			appDB::addAlert($_POST['email'],$_POST['target'],$_POST['newsletter']);
		} else {
			$f3->set('content','alerts.htm');
			$f3->set('message','Invalid email address');
		}
		echo View::instance()->render('layout.htm');
	});

$f3->route('GET /verify/@code',
	function($f3) {
		$code = $f3->get('PARAMS.code');
		if (appDB::verifyAlert($code)) {
			$f3->set('content','alert-verified.htm');
		} else {
			$f3->set('content','alert-notverified.htm');
		}
		echo View::instance()->render('layout.htm');
	});

$f3->route('GET /contact',
	function($f3) {
		$f3->set('content','contact.htm');
		echo View::instance()->render('layout.htm');
	});

$f3->route('GET /about',
	function($f3) {
		$f3->set('content','about.htm');
		echo View::instance()->render('layout.htm');
	});

$f3->run();
