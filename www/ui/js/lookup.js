function lookup(domain,id) {
	setStatus(0,id);
	var message='';
	$.ajax({
		url: "/status/"+domain,
		dataType: "json",
		success: function( response) {
			if (response.isRegistered) {
				setStatus(1,id);
			} else if (response.isBid) {
				setStatus(2,id);
			} else {
				setStatus(3,id);
			}
		}
	});
}

function setStatus(stat,id) {
	if (stat == 0) {
		message = '<i class="fa fa-spin fa-spinner"></i>';
	} else if (stat == 1) {
		message = '<a class="btn btn-sm btn-danger" disabled> <i class="fa fa-minus-circle"></i> </a>';
	} else if (stat == 2) {
		message = '<a class="btn btn-sm btn-warning" disabled> <i class="fa fa-money"></i> </a>';
	} else if (stat == 3) {
		message = '<a class="btn btn-sm btn-success" disabled> <i class="fa fa-thumbs-up"></i> </a>';
	}	
	$('div#status_'+id).html(message);
}
