<?php

	class appDB {
		static $limit = 100;
		static $predis;
		
		static function getDB() {
			if (!isset(self::$predis)) { 
				$f3 = Base::instance();
				self::$predis = new \Predis\Client([
					'scheme'=> $f3->get('redis.scheme'),
					'host'=> $f3->get('redis.host'),
					'port'=>$f3->get('redis.port'),
					'password'=>$f3->get('redis.password'),
					'database'=>$f3->get('redis.database')
				]);
			}
			return self::$predis;
		}

		static function searchSort($a,$b) {
			if ((int)$a['score'] >= (int)$b['score']) return -1;
			return 1;
		}

		static function getCount() {
			$db = static::getDB();
			return $db->get('domainCount');
		}

		static function getSearch($term,$page = 1) {
			$db = static::getDB();
			$sublist = array();
			$limit = static::$limit;			
			$skip = (($page - 1) * $limit);
			$res = $db->zrank('searchList',$term);
			if (is_numeric($res)) {
				$items = $db->zrange('searchList',$res,$res+500);
				foreach($items as $item) if (strstr( $item, "*" )) $sublist[] = substr($item,0,strlen($item)-1);
			}
			$total = count($sublist);
			$list = array_splice($sublist,$skip,$limit);
			$out = array();
			$trans = $db->transaction();
			foreach($list as $dom) {
				$trans->hgetall("domain:".$dom);
			}			
			$afterList = $trans->execute();
			if (is_null($afterList)) return array('list'=>array(),'total'=>0);
			usort( $afterList, "appDB::searchSort" );
			$final = array('list'=>$afterList,'total'=>$total);
			return $final;
		}

		//Get list of scored domains
		static function getDomains($page = 1) {
			$db = static::getDB();
			$limit = static::$limit;
			$items = $db->zrevrangebyscore('domainScores', '+inf', '-inf', array('LIMIT'=>array(($page-1)*$limit,$limit)));
			$trans = $db->transaction(); //Start a multi-transaction
			foreach($items as $id=>$key) {
				$trans->hgetall($key);
			}
			$result = $trans->execute();
			return $result;
		}

		//Get Page count
		static function getPages() {
			$db = static::getDB();
			$count = $db->zcount('domainList', '-inf' , '+inf');
			return floor($count / static::$limit);
		}

		static function getAC($term) {
			$outs = array();
			$db = static::getDB();
			$res = $db->zrank('searchList',$term);
			if (is_numeric($res)) {
				$completes = $db->zrange('searchList',$res,$res+100);
				foreach($completes as $k=>$comp) {
					if (substr($comp,-1) == "*") {
						$outs[] = substr($comp,0,strlen($comp)-1);
					}
				}
				return $outs;
			} else {
				return false;
			}	
		}


		static function getDomain($domain) {
			$db = static::getDB();
			$item = $db->hgetall('domain:'.$domain);
			return $item;
		}
/*

		static function removeDomain($domain) {
			$db = static::getDB();
			$col = $db->selectCollection('domains');
			$g = $col->remove(array('domain'=>$domain),array('justOne'=>true));
		}

		static function getId($id) {
			$db = static::getDB();
			$col = $db->selectCollection('domains');
			$cur = $col->findOne(array('_id'=>$id));
			return $cur;
		}

		static function getStatus($id) {
			$db = static::getDB();
			$col = $db->selectCollection('status');
			if ($cur = $col->findOne(array('domain_id'=>$id))) {
				return $cur;
			} else {
				return false;
			}
		}

		static function saveStatus($status,$id) {
			$db = static::getDB();
			$col = $db->selectCollection('status');
			$status->created = time();
			$status->domain_id = $id;
			$col->createIndex(array('created'=>1),array('expireAfterSeconds'=>(15*60*60)));
			$col->insert($status);
		}


		static function addAlert($email,$target,$newsletter) {
			$f3 = Base::instance();
			$alert = new stdClass();
			$db = static::getDB();
			$col = $db->selectCollection('alertsTemp');
			$col->createIndex(array('email'=>1,'verification'=>1));
			$col->createIndex(array('created'=>1),array('expireAfterSeconds'=>(24*60*60)));
			$alert->created = time();
			$alert->email = $email;
			$alert->verification = md5(uniqid());
			$alert->target = $target;
			$alert->newsletter = $newsletter;
			$col->insert($alert);
			//Send out validation email, once validated remove the verification code.
			$message = "
	Thank you for using Domainomatics alert system. You have asked to subscribe to the following alert :

		* {$target} \n
		
	If you have initated this request then please http://www.domainomatics.com/verify/{$alert->verification} Verify your subscription  

		~ Domainomatics management
";
			$smtp = new SMTP ( $f3->get('mail.host'), $f3->get('mail.port'), $f3->get('mail.scheme'), $f3->get('mail.username'), $f3->get('mail.password'));
			$smtp->set('To', $email );
			$smtp->set('Subject','Domainomatics alert validation');
			$smtp->set('From', 'info@domainomatics.com');
			$smtp->send($message,true);
		}

		static function verifyAlert($code) {
			$alert = new stdClass();
			$db = static::getDB();
			$col = $db->selectCollection('alertsTemp');
			if ($cur = $col->findOne(array('verification'=>$code))) {
				$col->remove(array('verification'=>$code));
				$col = $db->selectCollection('alerts');
				$col->createIndex(array('email'=>1,'verification'=>1));
				$alert->created = time();
				$alert->email = $cur['email'];
				$alert->verification = md5(uniqid());
				$alert->target = $cur['target'];
				$alert->newsletter = $cur['newsletter'];
				$col->insert($alert);
				return true;
			} else {
				return false;
			}
		}
*/

	}
?>
