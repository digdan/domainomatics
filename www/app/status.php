<?php
	class appStatus {
		static function getStatus($domain) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://instantdomainsearch.com/all/".$domain);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			$output = curl_exec($ch);
			curl_close($ch);
			$outputs = explode("\n",$output);
			foreach($outputs as $line) {
				$line = trim($line);
				return json_decode($line);
			}
		}
	}
?>
